{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Text Classification - Naive Bayes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Model klasifikasi Naive Bayes adalah suatu model probabilistik untuk kasus klasifikasi linear, yang berdasarkan pada teorema Bayes.\n",
    "Dikenal sebagai `Naive` model karena model ini mendasarkan pada asumsi bahwa fitur - fitur yang ada dalam data bersifat independen satu sama lain. \n",
    "Asumsi ini terkesan tidak realistis, karena dalam kenyataanya sangat jarang ditemui data dengan karakteristik tersebut.\n",
    "Meskipun begitu, dalam prakteknya Naive Bayes memiliki performa yang cukup bagus [1] dan telah sekian lama menjadi metode `baseline` dalam domain text classification.\n",
    "Implementasinya pun cukup mudah dan scalable sehingga banyak juga diterapkan pada domain lain."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Naive Bayes Graphical Representation](files/naive-bayes-graphical.png)\n",
    "*image from: Bishop - PRML*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Di atas adalah representasi graphical model dari Naive Bayes, dimana variabel $\\mathbf{z}$ adalah label dari class dan variabel $x_1 \\dots x_D$ merepresentasikan fitur yang saling independen (ditandai dengan tidak adanya hubungan / garis yang menghubungkan di antara variabel $x$.\n",
    "\n",
    "Dalam kasus sentiment classification, variabel $\\mathbf{z}$ merepresentasikan label sentimen sedangkan $x_1 \\dots x_D$ adalah kata - kata yang muncul dalam satu dokumen.\n",
    "Dengan kata lain, Naive Bayes memakai asumsi bahwa penulis dokumen tersebut dalam memilih kata ke-$i$ tidak bergantung kepada pilihan kata sebelum - sebelumnya."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Teorema Bayes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Teorema Bayes, sederhananya, mendeskripsikan bagaimana kita mengubah tingkat kepercayaan kita terhadap suatu kejadian (prior `->` posterior) ketika ada data baru (evidence).\n",
    "\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "\\text{posterior probability} &=& \\frac{\\text{likelihood} \\cdot \\text{prior probability}}{\\text{evidence}} \\\\\n",
    "P(y \\mid \\mathbf{x}) &=& \\frac{P(\\mathbf{x} \\mid y) P(y)}{P(\\mathbf{x})} \\\\\n",
    "\\end{eqnarray}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Likelihood merepresentasikan bagaimana \"kenampakan\" / distribusi data, jika telah diketahui kondisi tertentu (variabel sentimen $y$).\n",
    "Dalam beberapa literatur, istilah likelihood sering ditulis dengan istilah conditional probabilitas.  Sedangkan istilah evidence merujuk pada probabilitas munculnya evidence terlepas dari nilai prior atau posterior."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Contoh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Kita akan melakukan klasifikasi apakah dokumen **\"Titanic adalah film yang sangat bagus.\"** memiliki sentimen positif atau negatif.\n",
    "Jika kita merujuk pada teorema Bayes, maka kasus di atas dapat dinotasikan sebagai berikut:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "\\begin{eqnarray} \n",
    "P({\\scriptsize\\text{sentimen} \\mid \\text{dokumen}}) &=& \\frac{P({\\scriptsize\\text{dokumen} \\mid \\text{sentimen}})\\, P({\\scriptsize\\text{sentimen}})}{P(\\scriptsize\\text{dokumen})}\n",
    "\\end{eqnarray}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Variabel **`sentimen`** memiliki dua kemungkinan nilai, yaitu **`positif`** dan **`negatif`**.\n",
    "Untuk itu kita perlu menghitung probabilitas dari untuk kedua nilai tersebut."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "\\begin{eqnarray}\n",
    "P({\\scriptsize\\text{sentimen}=\\text{positif} \\mid \\text{dokumen} = \\text{Titanic adalah film yang sangat bagus}}) &=& \\frac{P({\\scriptsize\\text{dokumen} = \\text{Titanic adalah film yang sangat bagus} \\mid \\text{sentimen} = \\text{positif}})\\, P({\\scriptsize\\text{sentimen} = \\text{positif}})}{P(\\scriptsize\\text{dokumen} = {\\text{Titanic adalah film yang sangat bagus}})}\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "P({\\scriptsize\\text{sentimen}=\\text{negatif} \\mid \\text{dokumen} = \\text{Titanic adalah film yang sangat bagus}}) &=& \\frac{P({\\scriptsize\\text{dokumen} = \\text{Titanic adalah film yang sangat bagus} \\mid \\text{sentimen} = \\text{negatif}})\\, P({\\scriptsize\\text{sentimen} = \\text{negatif}})}{P(\\scriptsize\\text{dokumen} = {\\text{Titanic adalah film yang sangat bagus}})}\n",
    "\\end{eqnarray}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- **`posterior`** didefinisikan sebagai berapa probabilitas dari variabel `sentimen` yang bernilai `positif` / `negatif` jika diberikan dokumen \"Titanic adalah film yang sangat bagus\".\n",
    "\n",
    "- **`likelihood`** didefinisikan sebagai berapa probabilitas kemunculan dokumen \"Titanic adalah film yang sangat bagus\" jika diketahui variabel `sentimen` bernilai `positif` / `negatif`.\n",
    "\n",
    "- **`prior`** didefinisikan sebagai berapa probabilitas variabel `sentimen` bernilai `positif` / `negatif`.\n",
    "\n",
    "- **`evidence`** didefinisikan sebagai berapa probabilitas kemunculan dokumen \"Titanic adalah film yang sangat bagus\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Setelah kita mendapatkan probabilitas untuk masing - masing nilai pada variabel `sentiment`, keputusan akhir dapat ditentukan dari nilai yang memiliki probabilitas paling tinggi."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```\n",
    "IF\n",
    "    P(sentiment = positif | dokumen) > P(sentiment = negatif | dokumen)\n",
    "THEN\n",
    "    positif\n",
    "ELSE\n",
    "    negatif\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Jika kita cermati, ternyata probabilitas dari `evidence` atau $P(\\text{dokumen})$ bernilai sama untuk semua kemungkinan label sentimen.\n",
    "Hal ini dikarenakan perhitungan `evidence` tidak bergantung pada variabel `sentiment`.\n",
    "Nilainya hanya berfungsi sebagai faktor normalisasi sehingga nilai $P(\\text{sentiment} \\mid \\text{dokumen})$ valid sebagai fungsi probabilitas.\n",
    "Dalam kasus ini, perhitungan $P(\\text{dokumen})$ bisa diabaikan karena tidak akan berpengaruh pada keputusan akhir."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "\\begin{eqnarray} \n",
    "\\text{posterior} &\\propto& \\text{likelihood } \\times \\text{prior} \\\\\n",
    "P({\\scriptsize\\text{sentimen} \\mid \\text{dokumen}}) &\\propto& P({\\scriptsize\\text{dokumen} \\mid \\text{sentimen}})\\, P({\\scriptsize\\text{sentimen}})\n",
    "\\end{eqnarray}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Pertanyaan berikutnya adalah bagaimana kita menghitung likelihood dan prior?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Likelihood"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Naive Bayes merupakan metode machine learning dengan pendekatan generative learning.\n",
    "Untuk memecahkan masalah klasifikasi, Naive Bayes memulai dengan memodelkan bagaimana data dihasilkan.\n",
    "\n",
    "Dalam kasus ini kita akan mencoba memodelkan data mengikuti distribusi Multinomial.\n",
    "Yaitu di mana sebuah dokumen direpresentasikan sebagai sebuah vektor yang nilainya berupa frekuensi kemunculan kata (term frequency).\n",
    "Pada bagian sebelumnya, kita telah melakukan ekstraksi fitur berupa term frequency yang diberikan bobot dengan inverse document frequence (tf-idf).\n",
    "Hasilnya berupa matrik dengan dimensi $N \\times D$, dimana $N$ adalah jumlah dokumen dan $D$ adalah jumlah kata dalam vocabulary.\n",
    "\n",
    "Pemodelan data tersebut berdasar pada asumsi:\n",
    "- data sampel terdistribusi secara independen\n",
    "- fitur (kata) juga terdistribusi secara independen\n",
    "\n",
    "Seperti telah disebutkan di awal bahwa dalam melakukan prediksi, nilai `likelihood` didapat dari probabilitas kemunculan dokumen jika diketahui sentimen berupa positif / negatif.\n",
    "Nilai probabilitas ini dapat dihitung sebagai berikut:\n",
    "\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "likeliihood(c) &=& P(\\mathbf{x} \\mid y = c)  \\\\\n",
    "likelihood(c)  &=&  \\prod_{d=1}^D P(x_d \\mid y = c)^{m_d} \\\\\n",
    "likelihood(c)  &=&  \\prod_{d=1}^D \\left( \\frac{M_{cd}}{M_c} \\right)^{m_d} \\\\\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "dimana,\n",
    "\n",
    "- $P(\\mathbf{x} \\mid y = c)$ probabilitas munculnya dokumen baru $\\mathbf{x}$ jika diketahui sentimen adalah $c$\n",
    "- $P(x_d \\mid y = c)$ probabilitas munculnya kata $d$ jika diketahui sentimen adalah $c$\n",
    "- $M_{cd}$ jumlah kemunculan kata $d$ dari seluruh dokumen yang memiliki sentimen $c$\n",
    "- $M_{c}$ jumlah kata dari seluruh dokumen yang termasuk sentimen $c$\n",
    "- $m_{d}$ jumlah kemunculan kata $d$ pada dokumen baru $\\mathbf{x}$\n",
    "\n",
    "Nilai $M_{cd}$ and $M_{c}$ dihitung dari data training, sedangkan nilai $m_{d}$ dihitung dari dokumen yang akan diprediksi.\n",
    "Penentuan rumus estimasi di atas didapatkan dari [Maximum Likelihood Estimation](#Maximum-Likelihood-Estimation)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prior"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sedangkan nilai probabilitas prior dapat dihitung sebagai berikut:\n",
    "    \n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "prior &=&  P(y = c) \\\\\n",
    "prior &=&  \\frac{N_c}{N}\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "dimana:\n",
    "- $P(y = c)$ adalah probability munculnya dokumen dengan sentimen $c$\n",
    "- $N_{c}$ jumlah dokumen dengan label sentimen $c$\n",
    "- $N$ jumlah seluruh dokumen\n",
    "\n",
    "Nilai prior ini dihitung dari data training, sebagai informasi awal dalam kita melakukan prediksi.\n",
    "Penentuan rumus prior tersebut juga didapatkan dari hasil estimasi [berikut](#Prior)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Learning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pada tahap learning kita perlu menghitung:\n",
    "- Nilai probabilitas $P(x_d \\mid y)$ untuk seluruh kata dan untuk seluruh sentimen\n",
    "- Nilai probabilitas prior $P(y)$ untuk seluruh sentimen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prediksi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Untuk melakukan prediksi pada dokumen baru $\\mathbf{x}$, kita tinggal mengalikan nilai likelihood dan prior yang parameternya sudah kita hitung pada saat learning.\n",
    "\n",
    "\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "P(y = c \\mid \\mathbf{x}) &\\propto&  P(y = c) \\times P(\\mathbf{x} \\mid y = c) \\\\\n",
    "P(y = c \\mid \\mathbf{x}) &\\propto&  P(y = c) \\times \\prod_{d=1}^D P(x_d \\mid y = c)^{m_d} \\\\\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "Atau jika direpresentasikan dalam skala log\n",
    "\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "\\ln P(y = c \\mid \\mathbf{x}) &\\propto&  \\ln P(y = c) + \\sum_{d=1}^D m_d \\ln P(x_d \\mid y = c) \\\\\n",
    "\\end{eqnarray}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Implementasi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>SentenceId</th>\n",
       "      <th>Sentence</th>\n",
       "      <th>Indonesia</th>\n",
       "      <th>Sentiment</th>\n",
       "      <th>Processed</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>8082</td>\n",
       "      <td>Weighted down with slow , uninvolving storytel...</td>\n",
       "      <td>Tertimbang dengan lambat, omong kosong berceri...</td>\n",
       "      <td>0</td>\n",
       "      <td>timbang dengan lambat omong kosong cerita dan ...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>7560</td>\n",
       "      <td>A classy , sprightly spin on film .</td>\n",
       "      <td>Spin berkelas dan sigap di film.</td>\n",
       "      <td>1</td>\n",
       "      <td>spin kelas dan sigap di film</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>3831</td>\n",
       "      <td>Lazy filmmaking , with the director taking a h...</td>\n",
       "      <td>Pembuatan film malas, dengan sutradara mengamb...</td>\n",
       "      <td>0</td>\n",
       "      <td>buat film malas dengan sutradara ambil dekat h...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>5614</td>\n",
       "      <td>It is n't quite one of the worst movies of the...</td>\n",
       "      <td>Ini bukan salah satu film terburuk tahun ini.</td>\n",
       "      <td>0</td>\n",
       "      <td>ini bukan salah satu film buruk tahun ini</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>7549</td>\n",
       "      <td>Features nonsensical and laughable plotting , ...</td>\n",
       "      <td>Fitur plot yang tidak masuk akal dan menggelik...</td>\n",
       "      <td>0</td>\n",
       "      <td>fitur plot yang tidak masuk akal dan geli tunj...</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   SentenceId                                           Sentence  \\\n",
       "0        8082  Weighted down with slow , uninvolving storytel...   \n",
       "1        7560                A classy , sprightly spin on film .   \n",
       "2        3831  Lazy filmmaking , with the director taking a h...   \n",
       "3        5614  It is n't quite one of the worst movies of the...   \n",
       "4        7549  Features nonsensical and laughable plotting , ...   \n",
       "\n",
       "                                           Indonesia  Sentiment  \\\n",
       "0  Tertimbang dengan lambat, omong kosong berceri...          0   \n",
       "1                   Spin berkelas dan sigap di film.          1   \n",
       "2  Pembuatan film malas, dengan sutradara mengamb...          0   \n",
       "3      Ini bukan salah satu film terburuk tahun ini.          0   \n",
       "4  Fitur plot yang tidak masuk akal dan menggelik...          0   \n",
       "\n",
       "                                           Processed  \n",
       "0  timbang dengan lambat omong kosong cerita dan ...  \n",
       "1                       spin kelas dan sigap di film  \n",
       "2  buat film malas dengan sutradara ambil dekat h...  \n",
       "3          ini bukan salah satu film buruk tahun ini  \n",
       "4  fitur plot yang tidak masuk akal dan geli tunj...  "
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "data = pd.read_csv('files/text-processed.csv')\n",
    "data.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>0</th>\n",
       "      <th>1</th>\n",
       "      <th>2</th>\n",
       "      <th>3</th>\n",
       "      <th>4</th>\n",
       "      <th>5</th>\n",
       "      <th>6</th>\n",
       "      <th>7</th>\n",
       "      <th>8</th>\n",
       "      <th>9</th>\n",
       "      <th>...</th>\n",
       "      <th>3445</th>\n",
       "      <th>3446</th>\n",
       "      <th>3447</th>\n",
       "      <th>3448</th>\n",
       "      <th>3449</th>\n",
       "      <th>3450</th>\n",
       "      <th>3451</th>\n",
       "      <th>3452</th>\n",
       "      <th>3453</th>\n",
       "      <th>3454</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>...</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>...</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>...</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>...</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>...</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>5 rows × 3455 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "     0    1    2    3    4    5    6    7    8    9  ...   3445  3446  3447  \\\n",
       "0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  ...    0.0   0.0   0.0   \n",
       "1  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  ...    0.0   0.0   0.0   \n",
       "2  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  ...    0.0   0.0   0.0   \n",
       "3  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  ...    0.0   0.0   0.0   \n",
       "4  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  ...    0.0   0.0   0.0   \n",
       "\n",
       "   3448  3449  3450  3451  3452  3453  3454  \n",
       "0   0.0   0.0   0.0   0.0   0.0   0.0   0.0  \n",
       "1   0.0   0.0   0.0   0.0   0.0   0.0   0.0  \n",
       "2   0.0   0.0   0.0   0.0   0.0   0.0   0.0  \n",
       "3   0.0   0.0   0.0   0.0   0.0   0.0   0.0  \n",
       "4   0.0   0.0   0.0   0.0   0.0   0.0   0.0  \n",
       "\n",
       "[5 rows x 3455 columns]"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "features = pd.read_csv('files/text-feature.csv')\n",
    "features.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# convert labels and features into 2D array\n",
    "labels = data.Sentiment.as_matrix()\n",
    "features_matrix = features.as_matrix()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Maximum Likelihood Estimation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Adalah suatu metode untuk mencari parameter optimal dari suatu distribusi dari data yang diberikan.\n",
    "Dalam kasus ini, kita akan mencari parameter yang optimal untuk distribusi Multinomial yang kita pakai untuk memodelkan data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "$$\n",
    "\\begin{eqnarray} \n",
    "\\text{likelihood} &=& P(X \\mid y) \\\\\n",
    "&=& \\prod_{n=1}^N \\prod_{d=1}^D P(x_{nd} \\mid y) \\\\\n",
    "&=& \\prod_{n=1}^N \\prod_{c=1}^C \\prod_{d=1}^D \\theta_{cd}^{m_{nd} \\mathbb{I}(y_n = c)} \\\\ \n",
    "&=& \\prod_{c=1}^C \\prod_{d=1}^D \\theta_{cd}^{M_{cd}}\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "\\text{log-likelihood}\n",
    "&=& \\sum_{c=1}^C \\sum_{d=1}^D M_{cd} \\ln \\theta_{cd}\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "Distribusi Multinomial memiliki sifat\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "\\sum_{d=1}^D \\theta_{d} &=& 1\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "Sehingga kita perlu menambahkan Lagrange multiplier dalam fungsi Log-likelihood\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "\\text{log-likelihood}\n",
    "&=& \\sum_{c=1}^C \\sum_{d=1}^D M_{cd} \\ln \\theta_{cd} + \\lambda \\left( \\sum_{d=1}^D \\theta_{cd} - 1 \\right)\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "Hitung turunan fungsi log-likelihood terhadap variabel $\\theta_{cd}$\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "\\frac{\\partial \\text{log-likelihood}}{\\partial \\theta_{cd}} &=& 0 \\\\\n",
    "\\frac{M_{cd}}{\\theta_{cd}} + \\lambda\n",
    "&=& 0 \\\\\n",
    "M_{cd} &=& -\\lambda \\theta_{cd} \\\\\n",
    "\\sum_d^D M_{cd} &=& -\\lambda \\sum_d^D \\theta_{cd} \\\\\n",
    "-M_{c} &=& \\lambda \\\\\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "substitusikan nilai $\\lambda$\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "M_{cd} &=& -\\lambda \\theta_{cd} \\\\\n",
    "M_{cd} &=& M_c \\theta_{cd} \\\\\n",
    "\\theta_{cd} &=& \\frac{M_{cd}}{M_c}\\\\\n",
    "\\end{eqnarray}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "dimana:\n",
    "- $N$ adalah jumlah total dokumen\n",
    "- $D$ adalah jumlah total kata dalam vocabulary\n",
    "- $P(x_{nd} \\mid y)$ adalah probabilitas kata $d$ pada dokumen $n$, jika diketahui $y$\n",
    "- $\\theta_{cd}$ adalah probabilitas kemunculan kata $d$ jika diberikan label sentimen $y$ = $c$\n",
    "- $M_{cd}$ jumlah total kemunculan kata $d$ dari dokumen yang termasuk kelas $c$\n",
    "- $M_{c}$ jumlah total seluruh kata dari dokumen yang termasuk kelas $c$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prior"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Asumsikan kita definisikan prior $P(y)$ = $\\pi$.\n",
    "Kita dapat mengestimasi nilai prior juga dengan cara maximum likelihood estimation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "$$\n",
    "\\begin{eqnarray} \n",
    "\\text{prior} &=& P(y) \\\\\n",
    "&=& \\prod_{n=1}^N \\prod_{c=1}^C \\pi_c^{\\mathbb{I}(y_n = c)} \\\\ \n",
    "&=& \\prod_{c=1}^C \\pi_{c}^{N_{c}}\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "\\text{log-likelihood}\n",
    "&=& \\sum_{c=1}^C N_{c} \\ln \\pi_{c}\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "Probabilitas prior memiliki sifat sebagai berikut:\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "\\sum_{c=1}^C \\pi_{c} &=& 1\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "Sehingga kita perlu menambahkan Lagrange multipler dalam fungsi Log-likelihood\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "\\text{log-likelihood}\n",
    "&=& \\sum_{c=1}^C N_{c} \\ln \\pi_{c} + \\lambda \\left( \\sum_{c=1}^C \\pi_{c} - 1 \\right)\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "Hitung turunan fungsi di atas terhadap variabel $\\pi_{c}$:\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "\\frac{\\partial \\text{log-likelihood}}{\\partial \\pi_{c}} &=& 0 \\\\\n",
    "\\frac{N_{c}}{\\pi_{c}} + \\lambda\n",
    "&=& 0 \\\\\n",
    "N_{c} &=& -\\lambda \\pi{c} \\\\\n",
    "\\sum_c^C N_{c} &=& -\\lambda \\sum_c^C \\pi_{c} \\\\\n",
    "-N &=& \\lambda \\\\\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "kemudian substitusikan nilai $\\lambda$\n",
    "$$\n",
    "\\begin{eqnarray} \n",
    "N_{c} &=& -\\lambda \\pi_{c} \\\\\n",
    "N_{c} &=& N \\pi_{c} \\\\\n",
    "\\pi_{c} &=& \\frac{N_{c}}{N}\\\\\n",
    "\\end{eqnarray}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "dimana:\n",
    "- $\\pi_c$ adalah prior probability untuk kelas $c$\n",
    "- $N_{c}$ jumlah dokumen dengan label kelas $c$\n",
    "- $N$ jumlah total seluruh dokumen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References and External Resources"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[1]  Irina Rish. An empirical study of the naive bayes classifier. In IJCAI 2001 workshop on empirical methods in artificial intelligence , pages 41–46, 2001.\n",
    "[2]  Maximum likelihood estimation. (2017, May 12). In Wikipedia, The Free Encyclopedia. Retrieved 17:01, May 14, 2017, from https://en.wikipedia.org/w/index.php?title=Maximum_likelihood_estimation&oldid=780059567"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
