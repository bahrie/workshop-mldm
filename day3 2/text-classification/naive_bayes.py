from __future__ import division
import numpy as np
from sklearn.model_selection import cross_val_score


class NaiveBayes:

    def __init__(self):
        # constructor
        self.sentiment_labels = [0, 1]  # 0 negatif, 1 positif
        self.prior = [None, None]  # P(y)
        self.theta = [None, None]  # P(x_d | y)

    def set_params(self, **params):
        # ignore this
        pass

    def get_params(self, deep=True):
        # ignore this
        return {}

    def fit(self, features, target):
        # algoritma learning
        # features: matrix NXD (dokumen x kosakata)
        # target: array ukuran N dengan nilai 0 (negatif) atau 1 (positif)

        for label in self.sentiment_labels:
            # 0. cari semua dokumen yg termasuk sentimen c
            index = np.argwhere(target == label).flatten()
            # hasilnya label = 0 --> index = [1, 3]
            # hasilnya label = 1 --> index = [0, 2]

            # 1. Hitung nilai
            # M_cd =  jumlah kemunculan kata d dari seluruh dokumen yang memiliki sentimen c
            # M_c = jumlah kata dari seluruh dokumen yang termasuk sentimen c
            M_cd = 1 + features[index].sum(axis=0)
            M_c = M_cd.sum()

            # 2. Hitung nilai theta = P(x_d | y)
            # M_cd / M_c
            self.theta[label] = M_cd / float(M_c)

            # 3. Hitung nilai
            # N_c = jumlah dokumen yg termasuk sentimen c
            # N = jumlah seluruh dokumen
            N_c = index.shape[0]
            N = target.shape[0]

            # 4. Hitung nilai P(y)
            # N_c / N
            self.prior[label] = N_c / float(N)

    def predict(self, features):
        # features: matrix NXD (dokumen x kosakata)
        # features == m_d

        log_posterior = [None, None]
        # 1. Hitung log posterior untuk semua sentiment
        # log posterior = log prior + log likelihood
        for label in self.sentiment_labels:
            log_prior = np.log(self.prior[label])
            log_likelihood = np.sum(features * self.theta[label], axis=1)
            log_posterior[label] = log_prior + log_likelihood

        # 2. Hitung prediksi
        # Cari sentimen label yg memiliki log posterior paling tinggi
        sentiment = []
        for i in range(features.shape[0]):
            if log_posterior[0][i] > log_posterior[1][i]:
                sentiment.append(0)
            else:
                sentiment.append(1)
        
        return sentiment

    def score(self, features, target):
        # fungsi untuk menghitung akurasi
        # features: matrix NXD (dokumen x kosakata)
        # target: array ukuran N dengan nilai 0 (negatif) atau 1 (positif)

        # 1. predict
        prediction = self.predict(features)

        # 2. Bandingkan prediksi yang sama target
        accuration = len(prediction) - np.sum(abs(prediction - target))

        # 3. Akurasi
        return accuration / float(len(prediction))


    def cross_validation(self, features, target):
        # features: matrix NXD (dokumen x kosakata)
        # target: array ukuran N dengan nilai 0 (negatif) atau 1 (positif)
        pass


if __name__ == '__main__':

    test_data = np.array([[1, 2, 0, 0], 
                        [1, 0, 1, 1], 
                        [1, 1, 0, 2], 
                        [0, 0, 2, 1]])
                        
    target = np.array([1, 0, 1, 0])

    naivebayes = NaiveBayes()
    naivebayes.fit(test_data, target)
    #print(naivebayes.theta)
    #print(naivebayes.prior)

    result = naivebayes.predict(np.array([[1, 1, 0, 1], [1, 0, 2, 2]]))
    print(result)

    print(naivebayes.score(np.array([[1, 1, 0, 1], [1, 0, 2, 2]]), np.array([0, 0])))
