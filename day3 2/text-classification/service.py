# service.py

import pandas as pd
from flask import Flask, request
from naive_bayes import NaiveBayes
from features_extraction import FeatureExtractor
from preprocessing import PreProcessor
app = Flask("text-classification")


naive_bayes = NaiveBayes()
fe = FeatureExtractor()
pp = PreProcessor()


# localhost:5000/
@app.route("/")
def home():
    html = "<h1>Homepage</h1>"
    html += "<br>"
    html += "halaman 1"
    return html

# localhost:5000/classify?text=<inputan user>
@app.route("/classify")
def classify():
    text = request.args.get("text", "ini teks")

    if naive_bayes.theta[0] is None:
        return "Model belum ditraining."

    sentiment = text_classify(text)
    output = "<h1>Ini halaman classify</h1>"
    output += "<br>"
    output += "text = " + text
    output += "<br>"
    output += "Hasil klasifikasi = " + sentiment
    return output


# localhost:5000/train
@app.route("/train")
def train():
    # 1. baca file
    data = pd.read_csv("id-preprocess.csv")
    # 2. preprocessing
    # 3. features extraction (tf-idf)
    features = fe.fit(data.preprocess_result.tolist())
    target = data.Sentiment.as_matrix()
    
    # 4. train
    naive_bayes.fit(features, target)
    
    output = "theta = " + str(naive_bayes.theta)
    output += "<br>"
    output += "prior = " + str(naive_bayes.prior)
    return output


def text_classify(text):
    # proses klasifikasi
    # 1. preprocessing
    result = pp.preprocess(str(text))
    
    # 2. features extraction
    features = fe.transform([result])
    
    # 3. predict
    prediction = naive_bayes.predict(features)
    
    if prediction[0] == 1:
        return "positive"
    else:
        return "negative"


if __name__ == "__main__":
    app.run(debug=True)
