import string
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory


class PreProcessor:

    def __init__(self):
        self.stemmer = StemmerFactory().create_stemmer()
        self.remover = StopWordRemoverFactory().create_stop_word_remover()

    def preprocess(self, input_text):
        # 0 Hapus tanda baca
        text = ""
        for char in input_text:
            if char not in string.punctuation:
                text += char

        # 1 stemming
        text_stemmed = self.stemmer.stem(text)

        # 2 hapus stop words
        text_clean = self.remover.remove(text_stemmed)

        # 3 tokenization
        return text_clean
        # return text_clean.split(' ')
    

if __name__ == "__main__":
    import pandas as pd
    
    # baca data
    data = pd.read_csv('../files/id.csv')
    
    # inisialisasi object preprocessor
    preprocessor = PreProcessor()
    
    result = []
    for i, row in data.iterrows():
        result.append(preprocessor.preprocess(row['Indonesia']))
    
    data['preprocess_result'] = result
    data.to_csv('id-preprocess.csv')
