import numpy as np


class FeatureExtractor:

    def __init__(self):
        self.vocabulary = None
        self.idf = None

    def fit(self, train_data):
        # 1. Susun kosakata membentuk kamus
        # pangil method self.create_vocabulary
        self.create_vocabulary(train_data)

        # 2. Hitung frekuensi kemunculan kata
        # panggil method self.counts
        count_matrix = self.counts(train_data)

        # 3. Hitung tf
        # N_d = jumlah kata yang ada pada dokumen d
        # tf = count / N_d
        tf = count_matrix / count_matrix.sum(axis=1).reshape(-1, 1)

        # 4. Hitung idf
        # N = jumlah seluruh dokumen
        # N_d(t) = jumlah dokumen yang mengandung kata t
        # idf = log(N / N_d(t))
        # simpan idf sebagai attribute -> self.idf

        # 5. Hitung tf-idf
        return tf

    def transform(self, test_data):
        # 1. Hitung frekuensi kemunculan kata
        # panggil method self.counts
        count_matrix = self.counts(test_data)

        # 2. Hitung tf
        # N_d = jumlah kata yang ada pada dokumen d
        # tf = count / N_d
        tf = count_matrix / count_matrix.sum(axis=1).reshape(-1, 1)

        # 3. Hitung tf-idf
        # menggunakan idf yg sebelumnya telah dihitung dari data training
        # self.id
        return tf

    def create_vocabulary(self, data):
        vocabulary = set([])
        
        for i, row in enumerate(data):
            vocabulary.update(row.split(' '))
        
        self.vocabulary = list(vocabulary)

    def counts(self, data):
        # susun feature matrix N X M
        # N == jumlah dokumen
        # M == jumlah kata dalam kamus
        # hitung frekuensi kemunculan kata di setiap dokumen
        count_matrix = np.zeros((len(data), len(self.vocabulary)))
        
        # hitung kemunculan kata setiap dokumen
        for i, document in enumerate(data):
            for word in document:
                if word in self.vocabulary:
                    j = self.vocabulary.index(word)
                    count_matrix[i, j] += 1
        
        return count_matrix
    
