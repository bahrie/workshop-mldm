import numpy as np


class FeatureExtractor:

    def __init__(self):
        self.vocabulary = None
        self.idf = None

    def fit(self, train_data):
        # 1. Susun kosakata membentuk kamus
        # pangil method self.create_vocabulary

        # 2. Hitung frekuensi kemunculan kata
        # panggil method self.counts

        # 3. Hitung tf
        # N_d = jumlah kata yang ada pada dokumen d
        # tf = count / N_d

        # 4. Hitung idf
        # N = jumlah seluruh dokumen
        # N_d(t) = jumlah dokumen yang mengandung kata t
        # idf = log(N / N_d(t))
        # simpan idf sebagai attribute -> self.idf

        # 5. Hitung tf-idf

    def transform(self, test_data):
        # 1. Hitung frekuensi kemunculan kata
        # panggil method self.counts

        # 2. Hitung tf
        # N_d = jumlah kata yang ada pada dokumen d
        # tf = count / N_d

        # 3. Hitung tf-idf
        # menggunakan idf yg sebelumnya telah dihitung dari data training
        # self.id

    def create_vocabulary(self, data):
        vocabulary = set([])

    def counts(self, data):
