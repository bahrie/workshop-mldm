# service.py


from flask import Flask, request
app = Flask("text-classification")


# localhost:5000/
@app.route("/")
def home():
    return "Ini home page text classification"

# localhost:5000/classify?text=<inputan user>
@app.route("/classify")
def classify():
    text = request.args.get("text", "ini teks")
    sentiment = text_classify(text)

    output = "<h1>Ini halaman classify</h1>"
    output += "<br>"
    output += "text = " + text
    output += "<br>"
    output += "Hasil klasifikasi = " + sentiment
    return output


# localhost:5000/train
@app.route("/train")
def train():
    # 1. baca file
    # 2. preprocessing
    # 3. features extraction (tf-idf)
    # 4. train
    pass


def text_classify(text):
    # proses klasifikasi
    return "positive"


if __name__ == "__main__":
    app.run(debug=True)
