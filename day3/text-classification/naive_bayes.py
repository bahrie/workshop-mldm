import numpy as np
from sklearn.model_selection import cross_val_score


class NaiveBayes:

    def __init__(self):
        # constructor
        self.sentiment_labels = [0, 1]  # 0 negatif, 1 positif
        self.prior = [None, None]  # P(y)
        self.theta = [None, None]  # P(x_d | y)

    def set_params(self, **params):
        # ignore this
        pass

    def get_params(self, deep=True):
        # ignore this
        return {}

    def fit(self, features, target):
        # algoritma learning
        # features: matrix NXD (dokumen x kosakata)
        # target: array ukuran N dengan nilai 0 (negatif) atau 1 (positif)

        for label in self.sentiment_labels:
            # 0. cari semua dokumen yg termasuk sentimen c

            # 1. Hitung nilai
            # M_cd =  jumlah kemunculan kata d dari seluruh dokumen yang memiliki sentimen c
            # M_c = jumlah kata dari seluruh dokumen yang termasuk sentimen c

            # 2. Hitung nilai theta = P(x_d | y)
            # M_cd / M_c

            # 3. Hitung nilai
            # N_c = jumlah dokumen yg termasuk sentimen c
            # N = jumlah seluruh dokumen

            # 4. Hitung nilai P(y)
            # N_c / N

    def predict(self, features):
        # features: matrix NXD (dokumen x kosakata)

        # 1. Hitung log posterior untuk semua sentiment
        # log posterior = log prior + log likelihood

        # 2. Hitung prediksi
        # Cari sentimen label yg memiliki log posterior paling tinggi

    def score(self, features, target):
        # features: matrix NXD (dokumen x kosakata)
        # target: array ukuran N dengan nilai 0 (negatif) atau 1 (positif)

        # 1. predict

        # 2. Bandingkan prediksi yang sama target

        # 3. Akurasi

    def cross_validation(self, features, target):
        # features: matrix NXD (dokumen x kosakata)
        # target: array ukuran N dengan nilai 0 (negatif) atau 1 (positif)
